import { ThemeProvider } from '@material-ui/core';
import MailIcon from '@material-ui/icons/Mail';
import theme from './iconThemes'

export default function AddIcon(){
    return(
        <ThemeProvider theme={theme}>
            < MailIcon />
        </ThemeProvider>
    )
}
