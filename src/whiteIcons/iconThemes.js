import {createMuiTheme} from "@material-ui/core/styles";

const theme = createMuiTheme({
    overrides: {
        MuiSvgIcon:{
            root:{
                fill: "white",
                width: "20px",
                height: "20px",
                margin: "0 3px",
                cursor: "pointer"
            }
        }
    }
})


export default theme
