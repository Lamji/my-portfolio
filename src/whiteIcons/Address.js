import { ThemeProvider } from '@material-ui/core';
import HomeIcon from '@material-ui/icons/Home';
import theme from './iconThemes'

export default function AddIcon(){
    return(
        <ThemeProvider theme={theme}>
            <HomeIcon />
        </ThemeProvider>
    )
}
