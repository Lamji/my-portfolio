import { ThemeProvider } from '@material-ui/core';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import theme from './iconThemes'

export default function AddIcon(){
    return(
        <ThemeProvider theme={theme}>
            <PermContactCalendarIcon  />
        </ThemeProvider>
    )
}
