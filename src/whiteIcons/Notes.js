import { ThemeProvider } from '@material-ui/core';
import NotesIcon from '@material-ui/icons/Notes';
import theme from './iconThemes'

export default function AddIcon(){
    return(
        <ThemeProvider theme={theme}>
            <NotesIcon />
        </ThemeProvider>
    )
}
