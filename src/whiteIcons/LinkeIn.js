import { ThemeProvider } from '@material-ui/core';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import theme from './iconThemes'

export default function AddIcon(){
    return(
        <ThemeProvider theme={theme}>
            <LinkedInIcon />
        </ThemeProvider>
    )
}
