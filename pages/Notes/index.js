import React, { useState, useContext, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppHelper from '../../app-helper'
import Link from 'next/link'
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  WelcomeLoginUser: {
      textAlign: "left",
      margin: "6px 0",
      fontSize: "11px"
  },
  UserImage: {
      width: "30px",
      borderRadius: 20
  },
  UserName: {
      fontSize: "11px",
      marginLeft: "10px",
      color: "gray",
      fontWeight: "bold"
  },
  UserInfoHolder: {
    backgroundColor: "white",
    padding: "3px 10px",
    borderRadius: 10
  },
  LogoutButton: {
      fontSize: "11px",
      textDecoration: "none",
      color: "gray",
  }
}));

export default function CenteredGrid() {
  const classes = useStyles();

  const [userName, setUserName] = useState('')
  const [image, setImage] = useState('')
  const [email, setEmail] =useState('')
  const [loginType, setLogin] =useState('')

  useEffect(() =>{
      fetch(`${ AppHelper.API_URL }/users/details`,{
          headers:{
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
         console.log(data)
         setUserName(data.fullName)
         setEmail(data.email)
         setImage(data.image)
         setLogin(data.loginType)
      })

  },[])   

  return (
    <div className={classes.root}>
        <Grid item xs={12}>
          <Typography variant="body2" className={classes.WelcomeLoginUser}>
              Welcome
          </Typography>
          <Grid container item xs={12} className={classes.UserInfoHolder}
          direction="row"
          justify="flex-start"
          alignItems="center">
              <Grid item xs={8}
              container
              direction="row"
              justify="flex-start"
              alignItems="center">
                <img src={image} className={classes.UserImage} /> 
                <Typography variant="body2" className={classes.UserName}>
                    {userName}
                </Typography>
              </Grid>
              <Grid item xs={4}
                container
                direction="row"
                justify="flex-end"
                alignItems="center">
                <Link href="/logout">
                    <a className={classes.LogoutButton} role="button">Logout</a>
                </Link>
              </Grid>
          </Grid>
      </Grid>
    </div>
  );
}
