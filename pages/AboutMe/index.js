import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import Link from 'next/link'
import Linkein from '../../src/whiteIcons/LinkeIn'
import Facebook from '../../src/whiteIcons/facebook'


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  Main: {
    textAlign: 'center',
    color: "white",
    backgroundColor: '#5b5b5b',
    borderRadius: "0px",
    height: '100vh',
    padding: '0px',
    [theme.breakpoints.down('md')]: {
      height: "100%",
    },
  },
  Body:{
    height: '100%',
    backgroundColor: '#161616',
    borderRadius: '5px',
    textAlign: 'center'
  },
  Jumbotron: {
    height: "250px",
    backgroundImage: "url(/jumbo.png)",
  },
  AboutMeFeature: {
      padding: "10px",
      margin: "0 auto",
      marginTop: "20px",
      [theme.breakpoints.down('md')]: {
        margin: '0'
      },
  },
  img: {
      width: "100%"
  },
  img2: {
    width: "100%",
    height: "250px",
    [theme.breakpoints.down('md')]: {
      height: "100px",
    },
  },
  Title: {
      textAlign: "left",
      fontWeight: "bold",
      fontSize: "12px"
  },
  AboutMeInfo: {
      padding: '0 20px',
      
  },
  Text: {
      textAlign: "left",
      fontSize: "11px",
      margin: "12px 0"
  },
  Header: {
    fontWeight: "bold",
    fontSize: "12px",
    margin: '0 10px 0 0'
  },
  LinkHolder: {
    padding: "0 0 0 100px",
    height: "100px",
    [theme.breakpoints.down('md')]: {
      padding: '0',
      textAlign: "center"
    },
  },
  Button: {
    fontSize: "10px",
    margin: "0 10px"
  },
  FooterText: {
    fontSize: "11px",
    color: "gray",
    padding: "10px"
  },
  Footer: {
    marginTop: "20px",
    backgroundColor: "white"
  }
 
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12}>
          <Paper className={classes.Main}>
            <Grid item xs={12} className={classes.Body}>
                <Grid item xs={12} className={classes.Jumbotron}
                  container
                  direction="row"
                  justify="flex-start"
                  alignItems="center">
                    <Grid item xs={12} sm={4} container className={classes.LinkHolder}
                     direction="row"
                     justify="center"
                     alignItems="center">
                      <Typography variant="body1" className={classes.Header}>Find me here:</Typography>
                      <a href="https://www.linkedin.com/in/jick-lampago/" target="_blank"><Linkein /></a>
                      <Typography variant="body1" className={classes.Header}>LinkedIn</Typography>
                      <a href="https://www.facebook.com/mathew.lampago.3/" target="_blank"><Facebook/></a>
                      <Typography variant="body1" className={classes.Header}>Facebook</Typography>
                    </Grid>
                    <Grid xs={12}>
                    <Link href="/Projects">
                      <Button variant="contained" color="primary" className={classes.Button}>View My Projects</Button>
                    </Link>
                    <Link href="/">
                      <Button variant="contained" color="primary" className={classes.Button}>Back to home</Button>
                    </Link>
                    </Grid>
                </Grid>
                    <Grid item xs={12} sm={8} className={classes.AboutMeFeature}
                    container
                    direction="row"
                    justify="center"
                    alignItems="fles-start">
                        <Grid item xs={12} sm={4}>
                             <img  alt="complex" className={classes.img} src="https://scontent.fcrk1-1.fna.fbcdn.net/v/t1.6435-9/160891676_902188073866224_3272887494965276145_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=-DHHOFprNhMAX-BieF4&_nc_ht=scontent.fcrk1-1.fna&oh=f5e7b45672a6d06c93025ea506ec6777&oe=608CA990" />
                        </Grid>
                        <Grid item xs={12} sm={8} className={classes.AboutMeInfo}>
                            <Typography variant="body1" className={classes.Title}>
                                ABOUT ME
                            </Typography>
                            <Typography variant="body1" className={classes.Text}>
                                Hi, i graduated as <b>Associated in Computer Technology (ACT) </b> in Cataingan Polytchnic Institue last 2013, it is Ladderized Education but i decided not continue and work. From there while
                                working in different company i also work as freelancer (Graphic Designer and web developer). Last 2020 i enrolled on Zuitt Bootcamp to enhance my skills
                                and learn the latest trend technology and now i am a <b>Full Stack Web Developer</b>. I am a passionate web programmer, i can put 100% effort to my work in this field, open on cretics and still open to learn more technology.
                            </Typography>
                            <Typography variant="body1" className={classes.Title}>
                                SKILLS
                            </Typography>
                            <Typography variant="body1" className={classes.Text}>
                                Javascript - Node.js - Next.js - Express.js - React.js - MongoDB - React Bootstrap - Bootstrap 4 - Css 3 - Sass - Html 5 - Git Version Control - Material UI - REST API - Google API
                            </Typography>
                            <Typography variant="body1" className={classes.Title}>
                                KNOWLEDGE IN
                            </Typography>
                            <Typography variant="body1" className={classes.Text}>
                               Responsive Website - Adobe Photoshop - Figma - Mapbox - Graphql - JWT Authentication
                            </Typography>
                        </Grid>
                        <Grid item xs={12} className={classes.Footer}>
                          <Typography variant="body1" className={classes.FooterText}> Contact: 09206502183 || lampagojick5@gmail.com</Typography>
                        </Grid>
                    </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
