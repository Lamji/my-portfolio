import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Link from 'next/link'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    backgroundColor: "#393e46",
    color: theme.palette.text.secondary,
    borderRadius: 0,
    clipPath: "polygon(0 0, 100% 0, 90% 100%, 10% 100%)"
  },
  Link: {
      color: "white",
      margin: '0 5px',
      fontSize: "12px",
      textDecoration: "none",
      padding: "5px 10px",
  }
}));

export default function FullWidthGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container
      direction="row"
      justify="center"
      alignItems="center">
        <Grid item xs={12} sm={4}>
          <Paper className={classes.paper}>
          
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
