import { useContext, useEffect } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
	root: {
	  flexGrow: 1,
	},
	LogoutRoot: {
		backgroundColor: "#fafaf7",height: "100vh"
	},
	img: {
		margin: "200px auto",
		width: "15%"
	}
  }));

export default function index(){
	const classes = useStyles();
	const {unsetUser, setUser} = useContext(UserContext)

	useEffect(() => {
		setTimeout(function(){ 
			unsetUser();
			Router.push('/Home')
		}, 2500);
		
	}, [])

	return (
		<div className={classes.LogoutRoot}>
			<Grid container 
				direction="row"
				justify="center"
				alignItems="center">
				<img src="./logout.gif" className={classes.img} />
			</Grid>
		</div>
	)
}
	
