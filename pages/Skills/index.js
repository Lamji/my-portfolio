import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import Mern from '../mernstack/index'
import Knowledge from '../knowledge/index'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  SkillsTitle: {
    margin: "50px 0 50px 0"
  },
  CenterSection: {
    backgroundColor: "#dddddd",
    boxShadow: "none",
    width: "98%",
    margin: "0 auto",
    borderRadius: 10,
    [theme.breakpoints.down('md')]: {
      backgroundColor: "white",
    },
    [theme.breakpoints.down('xs')]: {
      backgroundColor: "white",
    },
  },
  Dev1: {
    margin: "0 10px",
    backgroundColor: "white",
    borderRadius: 10,
    color: "gray",
    
  },
  TextOne: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "#f5a25d",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
  TextTwo: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "#264de4",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
  TextThree: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "#CD6799",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
  TextFour: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "#563d7c",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
  TextFive: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "#03A9F4",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
  TextSix: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "#563d7c",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
  TextSeven: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "#CD6799",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
  TextEight: {
    borderRadius: 10,
    fontSize: "10px",
    width: "80%",
    backgroundColor: "black",
    padding: "0px",
    margin: "3px auto",
    padding: "1px 4px",
    color: "white"
  },
}));

export default function FullWidthGrid() {
  const classes = useStyles();

  return (
    
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12}>
          <Typography variant="h6" className={classes.SkillsTitle}>Skills</Typography>
          <Grid className={classes.CenterSection}
          container
          direction="row"
          justify="center"
          alignItems="center">
            <Grid item xs={12} sm={8} md={6}
            container
            direction="row"
            justify="center"
            alignItems="center">
              <Grid item xs={3} sm={4} md={2} className={classes.Dev1}>
                <Typography variant="body1" className={classes.TextOne}>
                  Html 5
                </Typography>
              </Grid>
              <Grid item xs={3} sm={2} md={2} className={classes.Dev1}>
              <Typography variant="body1" className={classes.TextTwo}>
                  Css 3
                </Typography>
              </Grid>
              <Grid item xs={3} sm={4} md={2} className={classes.Dev1}>
              <Typography variant="body1" className={classes.TextThree}>
                  Sass
                </Typography>
              </Grid>
              <Grid item xs={3} sm={4} md={2} className={classes.Dev1}>
              <Typography variant="body1" className={classes.TextFour}>
                  Bootstrap 4
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={8} md={6}
            container
            direction="row"
            justify="center"
            alignItems="center">
              <Grid item xs={4} sm={4} md={2} className={classes.Dev1}>
                <Typography variant="body1" className={classes.TextFive}>
                  Material UI
                </Typography>
              </Grid>
              <Grid item xs={4} sm={4} md={3} className={classes.Dev1}>
              <Typography variant="body1" className={classes.TextSix}>
                  React- Bootstrap
                </Typography>
              </Grid>
              <Grid item xs={3} sm={4} md={2} className={classes.Dev1}>
              <Typography variant="body1" className={classes.TextSeven}>
                  React-Sass
                </Typography>
              </Grid>
              <Grid item xs={3} sm={4} md={2} className={classes.Dev1}>
              <Typography variant="body1" className={classes.TextEight}>
                  Next.js
                </Typography>
              </Grid>
            </Grid>
          </Grid>
{/* MERN Stack Tech */}
          <Grid item xs={12}>
              <Mern />
          </Grid>
{/* Knowledge section */}
          <Grid item xs={12}>
            <Knowledge />
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
