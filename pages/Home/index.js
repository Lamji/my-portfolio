import React, { useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import Contact from '../../src/whiteIcons/contact'
import Mail from '../../src/whiteIcons/Email'
import Linkein from '../../src/whiteIcons/LinkeIn'
import Facebook from '../../src/whiteIcons/facebook'
import Location from '../../src/whiteIcons/Address'
import Skills from '../Skills/index'
import Card from '../shared/CardIndex'
import Notes from '../../src/whiteIcons/Notes'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'
import UserContext from '../../UserContext'
import AppHelper from '../../app-helper'


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    Main: {
        textAlign: 'center',
        color: "white",
        backgroundColor: '#161616',
        borderRadius: "0px",
        padding: '0px',
        [theme.breakpoints.down('md')]: {
            height: "100%",
        },
    },
    SideNav: {
        padding: "20px 50px",
        [theme.breakpoints.down('md')]: {
            padding: "0 10px",
        },
        [theme.breakpoints.down('xs')]: {
            padding: "0 50px",
            height: "100vh"
        },
    },
    Skills: {
        backgroundColor: "white",
        backgroundImage: "url(/lg.png)",
        backgroundRepeat: 'no-repeat',
        [theme.breakpoints.down('md')]: {
            backgroundImage: "url(/ipodpro.png)",
            backgroundRepeat: 'no-repeat',
        },
        [theme.breakpoints.down('sm')]: {
            backgroundImage: "url(/ipad.png)",
            backgroundRepeat: 'no-repeat',
        },
        [theme.breakpoints.down('xs')]: {
            backgroundImage: "url(/mobile.png)",
            backgroundRepeat: 'no-repeat',
            height: "100vh"
        },
    },
    RightNav: {
        backgroundColor: "#f7f7f7",
        color: "gray",
    },
    ImgHolder: {
        height: "100px",
        width: "100px",
        backgroundColor: "white",
        margin: "10px auto",
        borderRadius: 50,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "1px",
        [theme.breakpoints.down('xs')]: {
            width: "50px",
            height: "50px"
        },
    },
    img: {
        width: "100%",
        borderRadius: 50,
        [theme.breakpoints.down('xs')]: {
            width: "50px"
        },
    },
    Line: {
        margin: "20px 0",
        backgroundColor: "#dddddd",
        opacity: "0.4"
    },
    Line2: {
        margin: "8px 0",
        backgroundColor: "#dddddd",
        opacity: "0.2"
    },
    Name: {
        color: "gray"
    },
    SubName: {
        fontSize: "10px",
    },
    info: {
        fontSize: "11px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    Notes: {
        fontSize: "12px",
        textAlign: "left",
        marginTop: "20px"
    },
    Holder: {
        margin: "10px 0"
    },
    Link: {
        fontSize: "11px",
        color: "white",
        textDecoration: "none",
        backgroundColor: "#cf874e",
        padding: "3px 15px",
        borderRadius: 5
    },
    Header: {
        backgroundColor: "gray",
        borderRadius: 5,
        padding: "10px 16px",
        color: "white",
        fontWeight: "bold"
    },
    LiveProjects: {
        textAlign: "left",
        margin: " 20px 0px 0px 30px"
    },
    WorkExperience: {
        backgroundColor: "#d3e0ea",
        textAlign: "left",
        padding: "17px",
        color: "gray",
        height: "100%"
    },
    WorkExperienceContainer: {

        borderRadius: 5,
        marginTop: "10px",
        padding: "5px 10px"
    },
    WorkExperienceTitle: {
        fontWeight: "bold",
        fontSize: "12px",
        letterSpacing: "2px",
        marginBottom: "5px",
        marginTop: "10px"
    },
    WorkExperienceText: {
        fontSize: "11px"
    },
    infoNotes: {
        fontSize: "10px",
        textAlign: "left"
    },
    Googleroot: {
        '& :hover': {
            backgroundColor: "white",
        }
    },
    Googlebutton: {
        fontSize: "11px",
        color: "gray",
        display: "flex",
        alignItems: "left",
        justifyContent: "left",
        margin: "10px 0 30px 0",
        backgroundColor: "white",
        padding: "6px 10px",
        width: "100%"
    },
    GoogleIcon: {
        width: "20px",
        marginRight: "5px"
    },
    ErrorHandler: {
        width: "100%",
        height: "100vh",
        backgroundColor: "#f5f6f4"
    },
    Name: {
        fontSize: "11px"
    },
    loadingGif: {
        width: "15%"
    }
}));

export default function CenteredGrid() {
    const classes = useStyles();
    const { user, setUser } = useContext(UserContext)
    const [tokenId, setTokenId] = useState(null)
    const [name, setName] = useState('')


    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${accessToken}` }
        }

        fetch(`${AppHelper.API_URL}/users/details`, options).then(AppHelper.toJSON).then(data => {
            setUser({ email: data.email })
            Router.push('/profile')
        })

    }
    const authenticateGoogleToken = (response) => {
        setTokenId(response.tokenId)
        setName(response.profileObj.givenName)
        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    return (
        <div className={classes.root}>
            {tokenId == null ?
                <>
                    <Grid container className={classes.Main}>
                        <Grid item xs={12} sm={3} className={classes.SideNav}>
                            <hr className={classes.Line} />
                            <Grid container
                                direction="row"
                                justify="center"
                                alignItems="center">
                                <Grid item xs={3} sm={12}>
                                    <Grid item className={classes.ImgHolder}>
                                        <img alt="complex" className={classes.img} src="https://scontent.fcrk1-1.fna.fbcdn.net/v/t1.6435-9/160891676_902188073866224_3272887494965276145_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=-DHHOFprNhMAX-BieF4&_nc_ht=scontent.fcrk1-1.fna&oh=f5e7b45672a6d06c93025ea506ec6777&oe=608CA990" />
                                    </Grid>
                                </Grid>
                                <Grid item xs={6} sm={12}>
                                    <Typography variant="body1" className={classes.Name}>Jick Lampago</Typography>
                                    <Typography variant="body1" className={classes.SubName}>Full Stack Web Developer</Typography>
                                </Grid>
                            </Grid>
                            <hr className={classes.Line} />
                            <Grid container className={classes.Holder}>
                                <Contact />
                                <Typography className={classes.info}>09206502183</Typography>
                            </Grid>
                            <hr className={classes.Line2} />
                            <Grid container className={classes.Holder}>
                                <Mail />
                                <Typography className={classes.info}>lampagojick5@gmail.com</Typography>
                            </Grid>
                            <hr className={classes.Line2} />

                            <Grid container className={classes.Holder}>
                                <Grid item xs={8} container
                                    direction="row"
                                    justify="flex-start"
                                    alignItems="center">
                                    <Location />
                                    <Typography className={classes.info}>Valenzuela City</Typography>
                                </Grid>
                                <Grid item xs={4} container
                                    direction="row"
                                    justify="flex-end"
                                    alignItems="center">
                                    <a className={classes.Link} href="https://goo.gl/maps/Xnk4qFpMVnLK82527" target="_blank">Visit</a>
                                </Grid>
                            </Grid>
                            <hr className={classes.Line2} />
                            <Grid container className={classes.Holder}>
                                <Grid item xs={6} container
                                    direction="row"
                                    justify="flex-start"
                                    alignItems="center">
                                    <Linkein />
                                    <Typography className={classes.info}>LinkedIn</Typography>
                                </Grid>
                                <Grid item xs={6}
                                    container
                                    direction="row"
                                    justify="flex-end"
                                    alignItems="center">
                                    <a href="/" className={classes.Link} target="_blank">Visit</a>
                                </Grid>
                            </Grid>
                            <hr className={classes.Line2} />

                            <Grid container className={classes.Holder}>
                                <Grid item xs={6} container
                                    direction="row"
                                    justify="flex-start"
                                    alignItems="center">
                                    <Facebook />
                                    <Typography className={classes.info}>Facebook</Typography>
                                </Grid>
                                <Grid item xs={6}
                                    container
                                    direction="row"
                                    justify="flex-end"
                                    alignItems="center">
                                    <a href="/" className={classes.Link} target="_blank">Visit</a>
                                </Grid>
                            </Grid>
                            <hr className={classes.Line2} />

                            {/* Notes Code */}
                            <Grid item>
                                <Grid item xs={12} container
                                    className={classes.Notes}
                                    direction="row"
                                    justify="flex-start"
                                    alignItems="center">
                                    <Typography className={classes.NotesTitle}>ToDo Notes </Typography>
                                    <Notes />
                                </Grid>
                                <Grid >
                                    <Typography variant="body2" className={classes.infoNotes}>You must login first to create a notes.</Typography>
                                    <Grid item xs={12} className={classes.Googleroot}>
                                        <GoogleLogin
                                            clientId="1055007502137-5l881rml4392lgtccl7298h8butc9eoh.apps.googleusercontent.com"
                                            render={renderProps => (
                                                <Button onClick={renderProps.onClick} className={classes.Googlebutton}><img src="./Google.png" className={classes.GoogleIcon} />Login</Button>
                                            )}
                                            onSuccess={authenticateGoogleToken}
                                            onFailure={authenticateGoogleToken}
                                            cookiePolicy={'single_host_origin'}
                                            className="w-100 text-center d-flex justify-content-center"
                                        />
                                    </Grid>
                                </Grid>
                                <hr className={classes.Line2} />
                            </Grid>
                        </Grid>
{/* Skills section */}
                        <Grid item xs={12} sm={9} className={classes.RightNav} container>
                            <Grid item xs={12} className={classes.Skills}
                             container
                             direction="row"
                             justify="center"
                             alignItems="center">
                                <Skills />
                            </Grid>
{/* Live projects sections */}
                            <Grid item xs={12} sm={8} className={classes.Projects}>
                                <Typography variant="body1" className={classes.LiveProjects}>
                                    Live Personal Projects
                                </Typography>
                                <Card
                                    name="Covid-19 Tracker Dark"
                                    labelUsed="React.js - Next.js - React Bootsrap - React Sass - MapBox - JavaScrip Es6 - Public API"
                                    href="https://covid19-tracker-v-2-git-main-lamji.vercel.app/"
                                    hrefImg="https://covid19-tracker-v-2-git-main-lamji.vercel.app/"
                                    src="/Covid10Dark.png"
                                    alt="Covid Image"
                                />
                                <Card
                                    name="Covid-19 Tracker Light"
                                    labelUsed="React.js - Next.js - React Bootsrap - React Sass - MapBox - JavaScrip Es6 - Public API"
                                    src="/covidLight.png"
                                    alt="Covid Light Image"
                                    href="https://covid-19-tracker-bay-nu.vercel.app/?fbclid=IwAR1dfYqX7e4vQ1gWJ-aD1the4QHZQm5RWNM-vEuLnWtT6WZ7gnypqNJVR8s"
                                    hrefImg="https://covid-19-tracker-bay-nu.vercel.app/?fbclid=IwAR1dfYqX7e4vQ1gWJ-aD1the4QHZQm5RWNM-vEuLnWtT6WZ7gnypqNJVR8s"
                                />
                                <Card
                                    name=" Budget Tracker"
                                    labelUsed="React.js - Next.js - React Bootsrap - React Sass - MapBox - JavaScrip Es6 - Node.js - Express.js - Mongoose - MongoDB"
                                    src="/iTrack.png"
                                    alt="BudgetTracker Image"
                                    href="https://i-track.vercel.app/"
                                    hrefImg="https://i-track.vercel.app/"
                                />
                                <Card
                                    name="Youth Alive Blog"
                                    labelUsed="Html 5 - Css 3 - Sass - Bootstrap 4"
                                    src="/YAblog.png"
                                    alt="Youth Alive Image"
                                    href="https://lamji.gitlab.io/developer_pages/assets/pages/Genesis.html"
                                    hrefImg="https://lamji.gitlab.io/developer_pages/assets/pages/Genesis.html"
                                />
                            </Grid>
                            <Grid item xs={12} sm={4}>
{/* WorkExperience */}
                                <Grid item xs={12} className={classes.WorkExperience}>
                                    <Typography variant="body1">Work Experience</Typography>
                                    <Grid item className={classes.WorkExperienceContainer}>
                                        <Typography variant="body2" className={classes.WorkExperienceTitle}> &#9658; TACTICAL COORDINATOR</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Reckitt Benckister</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>January 2018 - August 2019</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Duties</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>
                                            &#8618; This not related to programming work. My main duty here was to maintain the product
                                            availability of a product in every store and to check
                                            the proper implementation of a prgram.
                            </Typography>
                                        <hr className={classes.Line} />

                                        <Typography variant="body2" className={classes.WorkExperienceTitle}> &#9658; FULL STACK WEB DEVELOPER</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Karlo Production - Valenzuela</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>August 2019 to October 2019</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Duties</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>
                                            &#8618; Personal porfolio for photography using Html, Css, Sass and Javascript.
                            </Typography>
                                        <hr className={classes.Line} />

                                        <Typography variant="body2" className={classes.WorkExperienceTitle}> &#9658; FULL STACK WEB DEVELOPER</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Kenn Flynn - Washington, DC</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>March 2020 to June 2020</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Duties</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>
                                            &#8618; Creating a website for their church including paypal integration using Html, Css, Sass and Javascript Es6.
                            </Typography>
                                        <hr className={classes.Line} />

                                        <Typography variant="body2" className={classes.WorkExperienceTitle}> &#9658; REACT DEVELOPER</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Codally Tech - Quezon City</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>November 2020 to December 2020</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>Duties</Typography>
                                        <Typography variant="body2" className={classes.WorkExperienceText}>
                                            &#8618; Creating an E – Learning platform website using React.js and Material UI. In this project I work on front end.
                            </Typography>
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Grid>
                    </Grid>
                </>
                :
                <Grid className={classes.ErrorHandler}
                    container
                    direction="column"
                    justify="center"
                    alignItems="center">
                    <Typography variant="body1" className={classes.Name}>Almost there {name}...</Typography>
                    <img src="./loading.gif" className={classes.loadingGif} />
                </Grid>
            }

        </div>
    );
}
