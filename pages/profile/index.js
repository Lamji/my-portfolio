import React, { useState,useContext, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import Contact from '../../src/whiteIcons/contact'
import Mail from '../../src/whiteIcons/Email'
import Link from 'next/link'
import Linkein from '../../src/whiteIcons/LinkeIn'
import Facebook from '../../src/whiteIcons/facebook'
import Location from '../../src/whiteIcons/Address'
import Card from '../shared/CardIndex'
import Notes from '../../src/whiteIcons/Notes'
import AppHelper from '../../app-helper'

const useStyles = makeStyles((theme) => ({
root: {
    flexGrow: 1,
},
container: {
    width: "90%",
    margin: "auto",
    borderRadius: 10,
    [theme.breakpoints.down('sm')]: {
       width: "100%"
      },
},
LeftNav: {
    backgroundColor: "#d3e0ea",
    padding: "0 10px"
},
UserName: {
    fontSize: "12px",
    marginLeft: "10px"
},
UserInfo: {
    fontSize: "11px",
    backgroundColor: "white",
    color: "gray",
    padding: "5px 10px",
    borderRadius: 5,
    margin: "10px 0"
},
UserImage: {
    width: "30px",
    borderRadius: 20
},
LogoutButton: {
    color: "gray",
    textDecoration: "none"
},
ErrorHandler: {
    width: "100%",
    height: "100vh",
    backgroundColor: "#f5f6f4"
},
loadingGif: {
    width: "15%"
},
Name: {
    fontSize: "11px"
},
}));

export default function CenteredGrid() {
  const classes = useStyles();
  const [userName, setUserName] = useState('')
  const [image, setImage] = useState('')
  const [isLogin,setIsLogin] =useState(false)


  useEffect(() =>{
    fetch(`${ AppHelper.API_URL }/users/details`,{
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        if(data.auth != "failed"){
              
                setUserName(data.fullName)
                setImage(data.image)
                setIsLogin(true)
         
        }else{
            setIsLogin(false)
        }
    })
   
},[isLogin])   

  return (
    <div className={classes.root}>
        {isLogin ? 
        <React.Fragment>
            <Grid className={classes.container}
            container
            direction="row"
            justify="center"
            alignItems="flex-start">
                <Grid item xs={12} sm={3} className={classes.LeftNav}
                container
                direction="row"
                justify="center"
                alignItems="center">
                    <Grid item xs={9}
                     container
                     direction="row"
                     justify="flex-start"
                     alignItems="center"
                     className={classes.UserInfo}>
                        <img src={image} className={classes.UserImage}></img>
                        <Typography variant="body1" className={classes.UserName}>{userName}</Typography>
                    </Grid>
                    <Grid item xs={3}
                      container
                      direction="row"
                      justify="center"
                      alignItems="center">
                        <Link href="/logout">
                            <a className={classes.LogoutButton} role="button">Logout</a>
                        </Link>
                    </Grid>
                </Grid>
{/* content */}
                <Grid item xs={12} sm={9}>

            
                </Grid>
            </Grid>
        </React.Fragment>
        : 
        <>
        <Grid className={classes.ErrorHandler}
        container
        direction="column"
        justify="center"
        alignItems="center">
        <Typography variant="body1" className={classes.Name}>Here we go...</Typography>
        <img src="./loading.gif" className={classes.loadingGif} />

    </Grid>   
        </>
        }
     
    </div>
  );
}
