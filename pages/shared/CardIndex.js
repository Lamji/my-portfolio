import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: '10px 0'
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 600,
    [theme.breakpoints.down('sm')]: {
      padding: "10px 50px"
    },
  },
  image: {
    width: 200,
    height: 128,
    [theme.breakpoints.down('sm')]: {
      width: "100%",
      height: "100%"
    },
  },
  img: {
    width: "100%",
    height: "100%"
 
  },
  Title: {
      textAlign: "left"
  },
  LabelUsesd: {
      fontSize: "11px"
  },
  Link: {
      color: "white",
      textDecoration: "none",
      backgroundColor: "#cf874e",
      padding: "5px 17px",
      fontSize: "11px",
      borderRadius: 10,
      '&:hover': {
        color: "#cf874e",
        backgroundColor: "white"
      }
  }
}));

export default function ComplexGrid(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper} elevation={0}>
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className={classes.image} href={props.hrefImg} target="_blank">
              <img className={classes.img} alt={props.alt} src={props.src} />
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs className={classes.Title}>
                <Typography gutterBottom variant="subtitle1">
                  {props.name}
                </Typography>
                <Typography variant="body2" gutterBottom>
                 Technology
                </Typography>
                <Typography variant="body2" color="textSecondary" className={classes.LabelUsesd}>
                  {props.labelUsed}
                </Typography>
              </Grid>
              <Grid item>
               <a href={props.href} className={classes.Link} target="_blank">GO TO THIS SITE</a>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}
