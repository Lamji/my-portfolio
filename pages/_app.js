import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../src/theme';
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';


export default function MyApp(props) {
  const { Component, pageProps } = props;
  const [user, setUser] = useState({
		email: null,
	
	})

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }

    const accessToken  = localStorage.getItem('token')
		const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
    }
    fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
      setUser({ email: data.email})
  })

  }, [user.email]);
  const unsetUser = () => {
		localStorage.clear();

		setUser({
			email: null,
		})
	}


  return (
    <React.Fragment>
      <Head>
        <title>Portfolio</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
    
        
      </ThemeProvider>
      <UserProvider value={{user, setUser, unsetUser}}>
	  	<Component {...pageProps} />
  	</UserProvider>
    </React.Fragment>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};
