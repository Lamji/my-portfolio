import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import Header from './aboutme'
import Link from 'next/link'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  Main: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: "white",
    backgroundColor: '#5b5b5b',
    borderRadius: "0px",
    height: '100vh',
    padding: '0px'
  },
  Body:{
    height: '100%',
    backgroundColor: '#161616',
    borderRadius: '5px',
    textAlign: 'center'
  },
  Header:{
    fontSize: '13px',
    paddingTop: '100px',
    letterSpacing: '3px'
  },
  SubHeader:{
    fontSize: '13px',
    paddingTop: '10px',
    color: '#cf874e',
    fontWeight: 'bold',
    letterSpacing: '10px'
  },
  Line:{
    width: '50px',
    borderColor: '#cf874e',
    borderWidth: 10
  },
  Line2:{
    borderColor: '#5b5b5b',
    width: "20px",
  },
  Title:{
    letterSpacing: '7px',
    marginTop: "60px",
    fontWeight: 'bold'
  },
  about: {
    fontSize: '11px'
  },
  Line3: {
    height: "130px",
    width: '100px',
    opacity: "0.4",
    clipPath: "polygon(30% 0, 71% 0, 70% 65%, 100% 65%, 51% 100%, 0 65%, 30% 64%)",
    margin: '0 auto',
    borderColor: '#C0C0C0',
    borderWidth: 50
  },
  Button:{
    color: "white",
    backgroundColor: "#5b5b5b",
    marginTop: '10px',
    fontSize: '12px',
    padding: "5px 10px"
  }
}));

export default function CenteredGrid() {
  const classes = useStyles();

  const [isOPen,setIsOpen] =useState(false)

  const handleChange = (e) => {
    e.preventDefault()
    console.log("setIsOPen to true")
  }

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12}>
          <Paper className={classes.Main}>
            <Grid item xs={12} className={classes.Body}>
            <Grid>
              <Header />
            </Grid>
              <Typography variant="body1" className={classes.Header}>
                JICK LAMPAGO
              </Typography>
              <hr className={classes.Line}/>
              <Typography variant="body1" className={classes.SubHeader}>
                BORN TO BE A
              </Typography>
              <hr className={classes.Line2}/>
              <Typography variant="h4" className={classes.Title}> 
                FULL STACK DEVELOPER
              </Typography>
              <Typography variant="body1" className={classes.about}>All you need to know about me</Typography>
              <hr className={classes.Line3}/>
              
              <Link href="/Home">
              <Button className={classes.Button}>Open</Button>
              </Link>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
